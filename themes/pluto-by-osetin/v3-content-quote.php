<div class="item-isotope <?php echo os_list_categories_for_filtering(get_the_ID()); ?>" <?php echo os_item_data(); ?>>
<article id="post-<?php the_ID(); ?>" <?php post_class('pluto-post-box'); ?>>
  <?php
	$term_arr = get_the_terms( get_the_ID(),'quote_author');
	if($term_arr){
		$term_id = $term_arr[0]->term_id;
		$author_name = $term_arr[0]->name;
		$author_link = get_term_link($term_id, 'quote_author');
		$term_image_arr = get_term_meta( $term_id, 'author_image', true);
		$term_image = $term_image_arr['guid'];
	}
	

  $post_thumbnail_arr = wp_get_attachment_image_src( get_post_thumbnail_id(), "post-thumbnail" );
  if($post_thumbnail_arr) {
    $css_style = "background-image: url(". $post_thumbnail_arr[0] .");";
  }else{
    $css_style = "";
  }
  ?>
  <div class="post-body" style="<?php echo $css_style; ?>">
	<?php osetin_top_social_share_index(); ?>
    <?php if(has_post_thumbnail()): ?>
      <div class="image-fader"></div>
    <?php endif?>
    <div class="quote-content">
      <div class="quote-icon"><i class="os-new-icon os-new-icon-quotes-left"></i></div>
      <h2 class="post-content entry-content"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo the_content(); ?></a></h2>
      <?php if ($author_name){ ?>
	  <a href ="<?php echo $author_link; ?>"> <img src="<?php echo $term_image; ?>" class="author_image"><span class="quote-author"> - <?php echo $author_name; ?></span></a>
	  <?php }else{
		  ?>
		  <img src="http://www.quotesschool.com/wp-content/plugins/userpro/img/default_avatar_male.jpg" class="author_image"><span class="quote-author"> - Anonymous </span>
	  <?php } ?>
	</div>
  </div>
  	 <?php if(os_is_post_element_active('date') || os_is_post_element_active('author') || os_is_post_element_active('like') || os_is_post_element_active('view_count')): ?>
      <div class="post-meta entry-meta">

        <?php global $show_author_face; ?>
        <?php if(!isset($show_author_face)) $show_author_face = false; ?>
        <?php if($show_author_face){ ?>


            <div class="meta-author-face">
              <div class="meta-author-avatar">
                <?php echo get_avatar(get_the_author_meta('ID')); ?>
              </div>
              <div class="meta-author-info">
                <div class="meta-author-info-by"><?php _e('Written by', 'pluto') ?></div>
                <div class="meta-author-name vcard"><strong><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) )) ; ?>" class="url fn n" rel="author"><?php echo get_the_author(); ?></a></strong></div>
              </div>
            </div>

        <?php }else{ ?>

          <?php if(os_is_post_element_active('category')): ?>
            <?php echo get_the_category_list(); ?>
          <?php endif; ?>
          <?php if(os_is_post_element_active('view_count')): ?>
            <div class="meta-view-count">
              <i class="fa os-icon-eye"></i>
              <span><?php if(function_exists('echo_tptn_post_count')) echo do_shortcode('[tptn_views]'); ?></span>
            </div>
          <?php endif; ?>

        <?php } ?>

        <?php if(os_is_post_element_active('like')){ ?>
          <div class="meta-like">
            <?php
            global $likes_type;
            if(!isset($likes_type)) $likes_type = 'regular';
            os_vote_build_button(get_the_ID(), '', false, $likes_type, get_the_permalink()); ?>
          </div>
        <?php } ?>

      </div>
    <?php endif; ?>
</article>
</div>