<div class="item-isotope <?php echo os_list_categories_for_filtering(get_the_ID()); ?>" <?php echo os_item_data(); ?>>
<article id="post-<?php the_ID(); ?>" <?php post_class('pluto-post-box'); ?>>
  <?php
	$term_arr = get_the_terms( get_the_ID(),'quote_author' );
	$author_link = $term_arr[0]->name;
	$term_id = $term_arr[0]->term_id;

	$author_link = get_term_link($term_id, 'quote_author');
	?><pre><?php print_r($author_link); ?></pre><?php

  $post_thumbnail_arr = wp_get_attachment_image_src( get_post_thumbnail_id(), "post-thumbnail" );
  if($post_thumbnail_arr) {
    $css_style = "background-image: url(". $post_thumbnail_arr[0] .");";
  }else{
    $css_style = "";
  }
  ?>
  <div class="post-body" style="<?php echo $css_style; ?>">
    <?php if(has_post_thumbnail()): ?>
      <div class="image-fader"></div>
    <?php endif?>
    <div class="quote-content">
      <div class="quote-icon"><i class="os-new-icon os-new-icon-quotes-left"></i></div>
      <h2 class="post-content entry-content"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo os_quote_excerpt(25); ?></a></h2>
      <a href ="<?php echo $author_link; ?>"><div class="quote-author">- <?php echo osetin_get_field('quote_author'); ?></div></a>
    </div>
  </div>
</article>
</div>